# ZADANIE TESTOWE - GIT #

W tym teście zapoznasz się z podstawami pracy w repozytoriach GIT.
Projekt składa się z trzech modułów, dwa z nich znajdują się na osobnych feature branchach.
**Docelowo** pliki modułów 2 i 3 (z branchy *feature2* oraz *feature3*)  mają zastąpić moduł 1.

## Przydatne komendy: ##

* Dodanie pliku (przed commitem) do repo: 
```git add <ŚCIEŻKA_DO_PLIKU_1> <ŚCIEŻKA_DO_PLIKU_2> ...```
* Commit pliku do repo: 
```git commit -m "<TRESC_WIADOMOSCI>```
* Zmiana brancha:
```git checkout <NAZWA_BRANCHA>```
* Utworzenie nowego brancha i przejście do niego:
```git checkout -b <NAZWA_BRANCHA>```
* Wypchnięcie zacommitowanych zmian:
```git push <NAZWA_REPOZYTORIUM_ZDALNEGO - najczęściej "origin"> <NAZWA_BRANCHA>```
* Dociągnięcie zmian z danego brancha:
```git pull <NAZWA_BRANCHA>```

Pełna lista przydatnych komend znajduje się [tutaj](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html).