function increment(module1) {
  console.log(module1);

  return module1++;
}

function decrement(module1) {
  return module1--;
}

module.exports = {
    increment,
    decrement
}